# A List is a collection which is ordered and changeable. Allows duplicate members.

# create list
numbers = [1, 2, 3, 4, 5]
fruits = ['Apples', 'Oranges', 'Grapes', 'Pears']

# use a constructor
#numbers2 = list((1, 2, 3, 4, 5))

# get a value
print(fruits[1])

# get length
print(len(fruits))

# append to list
fruits.append('Mangos')

# remove from list
fruits.remove('Mangos')

# insert into position
fruits.insert(2, 'Strawberries')

# change value
fruits[0] = 'Blueberries'

# remove with pop
fruits.pop(2)

# reverse list
fruits.reverse()

# sort list
fruits.sort()

# reverse sort
fruits.sort(reverse=True)

print(fruits)