# A Tuple is a collection which is ordered and unchangeable. Allows duplicate members.
 
# create tuple
fruits = ('Apple', 'Oranges', 'Grapes')
#fruits2 = tuple(('Apple', 'Oranges', 'Grapes'))

# single value needs trailing comma
fruits2 = ('Apples',)

# get value
#print(fruits[1])

# delete tuple
del fruits2

# get length
fruits = len(fruits)

#print(fruits)

# A Set is a collection which is unordered and unindexed. No duplicate members.

# create set
fruits_set = {'Apples', 'Oranges', 'Mango'}

# check if in set
#print('Apples' in fruits_set)

# add to set
fruits_set.add('Grapes')

# remove from set
fruits_set.remove('Grapes')

# add duplicate
fruits_set.add('Apples')

# clear set
#fruits_set.clear()

# delete
#del fruits_set

print(fruits_set)