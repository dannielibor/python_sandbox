# Strings in python are surrounded by either single or double quotation marks. Let's look at string formatting and some string methods

name = 'Brad'
age = 37

# concatenate
#print('Hello, my name is ' + name + ' and I am ' + str(age))

# String Formatting

# arguments by position
#print('My name is {name} and I am {age}'.format(name=name, age=age))

# f-string (3.6+)
#print(f'Hello, my name is {name} and I am {age}')

# String Methods

s = 'hello world'

# capitalize string
print(s.capitalize())

# make all uppercase
print(s.upper())

# make all lower
print(s.lower())

# swap case
print(s.swapcase())

# get length
print(len(s))

# replace
print(s.replace('world', 'everyone'))

# count
sub = 'l'
print(s.count(sub))

# starts with
print(s.startswith('hello'))

# ends with
print(s.endswith('d'))

# split into a list
print(s.split())

# find position
print(s.find('r'))

# is all alphanumeric
print(s.isalnum())

# is all alphabetic
print(s.isalpha())

# is all numeric
print(s.isnumeric())