# A Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.

# create dict
person = {
    'first_name': 'John',
    'last_name': 'Doe',
    'age': 30
}

# use constructor
#person2 = dict(first_name='Sara', last_name='Williams')

# get value
print(person['first_name'])
print(person.get('last_name'))

# add key/value
person['phone'] = '555-555-5555'

# get dict keys
print(person.keys())

# get dict items
print(person.items())

# copy dict
person3 = person.copy()
person3['city'] = 'Boston'

# remove item
del person['age']
person.pop('phone')

# clear
person.clear()

# get length
#print(len(person3))

# list of dict
people = [
    {'name': 'Martha', 'age': 30},
    {'name': 'Kevin', 'age': 25}
]

print(people[1]['name'])